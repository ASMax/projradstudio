//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TGridPanelLayout *gr;
	TImageList *im;
	TGlyph *gl0;
	TGlyph *gl1;
	TGlyph *gl2;
	TGlyph *gl3;
	TGlyph *gl4;
	TGlyph *gl5;
	TGlyph *gl6;
	TGlyph *gl7;
	TGlyph *gl8;
	TGlyph *gl9;
	TGlyph *gl10;
	TGlyph *gl11;
	TGlyph *gl12;
	TGlyph *gl13;
	TGlyph *gl14;
	TGlyph *gl15;
	TButton *buBegin;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
	TGlyph *Glyphs[16];
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
const int size = 16;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
