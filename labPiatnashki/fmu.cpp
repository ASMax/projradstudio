//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "labPiatnashkiPCH1.h"
#include <cstdlib>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	TGlyph* Glyphs[16] = {gl0, gl1, gl2, gl3, gl4, gl5, gl6, gl7, gl8, gl9, gl10, gl11, gl12, gl13, gl14, gl15};

	for (int i = 0; i < 15; i++) {
		Glyphs[i]->ImageIndex = i;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormClose(TObject *Sender, TCloseAction &Action)
{
	delete *Glyphs;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key == vkRight) {
		Glyphs[7]->ImageIndex = -1;
		Glyphs[8]->ImageIndex = 14;
	}

//	if (Key == vkLeft) {
//		Glyphs[14]->ImageIndex = 15;
//        Glyphs[15]->ImageIndex = -1;
//	}
//
//	if (Key == vkUp) {
//		Glyphs[13]->ImageIndex = -1;
//		Glyphs[15]->ImageIndex = 12;
//	}
//
//	if (Key == vkDown) {
//		Glyphs[13]->ImageIndex = 12;
//		Glyphs[15]->ImageIndex = -1;
//	}
}
//---------------------------------------------------------------------------

