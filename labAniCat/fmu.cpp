//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FCountCat = 0;
    FCountLuna = 0;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::imCatClick(TObject *Sender)
{
	FCountCat++;
	laCountCat->Text = "����� = " + IntToStr(FCountCat);
	floatAnimationCountCat->Start();
	//
}
//---------------------------------------------------------------------------
void __fastcall Tfm::imLunaClick(TObject *Sender)
{
	FCountLuna++;
	laCountLuna->Text = "���� = " + IntToStr(FCountLuna);
	floatAnimationCountLuna->Start();
	//
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormResize(TObject *Sender)
{
	floatAnimationCat->StopValue = this->Width - imCat->Width;
	floatAnimationLuna->StopValue = this->Width - imLuna->Width;
}
//---------------------------------------------------------------------------
