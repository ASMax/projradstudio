//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TImage *imCat;
	TImage *imLuna;
	TLabel *laCountCat;
	TLabel *laCountLuna;
	TFloatAnimation *floatAnimationCat;
	TFloatAnimation *floatAnimationLuna;
	TFloatAnimation *floatAnimationCountCat;
	TFloatAnimation *floatAnimationCountLuna;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall imCatClick(TObject *Sender);
	void __fastcall imLunaClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
private:	// User declarations
	int FCountCat;
	int FCountLuna;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
