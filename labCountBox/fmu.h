//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *layHeader;
	TButton *buAgain;
	TLabel *laHeader;
	TTabControl *tc;
	TLabel *laTime;
	TTabItem *tiAbout;
	TTabItem *tiHelp;
	TTabItem *tiMenu;
	TTabItem *tiPlay;
	TGridPanelLayout *gpFooter;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TLayout *latFooter;
	TGridPanelLayout *gpPlayField;
	TRectangle *Rectangle1;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TRectangle *Rectangle2;
	TTimer *tmPlay;
	TTabItem *tiSettings;
	TLayout *Layout1;
	TEdit *edHard;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buStart;
	TButton *buSettings;
	TButton *buExit;
	TButton *buBack;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall buAnswerAllClick(TObject *Sender);
	void __fastcall buAgainClick(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buSettingsClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	int GameTime;

	double FTimeValue;

	TList *FListBox;
	TList *FListAnswer;

	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();

	bool flag;

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};

const int cMaxBox = 25;
const int cMaxAnswer = 6;
const int cMinPossible = 4;
const int cMaxPossible = 14;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
