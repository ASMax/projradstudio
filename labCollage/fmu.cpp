//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------

__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void Tfm::SelectionAll(TObject *Sender)
{
	if (FSel != NULL) {
		FSel->HideSelection = true;
	}
	FSel = dynamic_cast<TSelection*>(Sender);
	if (FSel != NULL) {
        FSel->HideSelection = false;
	}

	tbOptions->Visible = (FSel != NULL);
	if (tbOptions->Visible) {
        trRotation->Value = FSel->RotationAngle;
	}

	tbImage->Visible = (FSel != NULL) && (dynamic_cast<TGlyph*>(FSel->Controls->Items[0]));

	tbRect->Visible = (FSel != NULL) && (dynamic_cast<TRectangle*>(FSel->Controls->Items[0]));
	if (tbRect->Visible) {
		ComboColorBox->Color = ((TRectangle*)FSel->Controls->Items[0])->Fill->Color;
		trRectRadius->Value = ((TRectangle*)FSel->Controls->Items[0])->XRadius;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FSel = NULL;
	SelectionAll(lay);

	Selection1->HideSelection = true;
	Selection2->HideSelection = true;
	Selection3->HideSelection = true;
	Selection4->HideSelection = true;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
    SelectionAll(Sender);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    ShowMessage("labCollage - Petrov Daniel 171-334");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBringToFrontClick(TObject *Sender)
{
	FSel->BringToFront();
	FSel->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSendToBackClick(TObject *Sender)
{
	FSel->SendToBack();
	FSel->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::trRotationChange(TObject *Sender)
{
    FSel->RotationAngle = trRotation->Value;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDeleteClick(TObject *Sender)
{
	FSel->DisposeOf();
	FSel = NULL;
	SelectionAll(lay);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buImagePrevClick(TObject *Sender)
{
	TGlyph *x  = (TGlyph*)FSel->Controls->Items[0];
	x->ImageIndex = ((int)x->ImageIndex <= 0) ? dm->il->Count - 1 : (int)x->ImageIndex - 1;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buImageNextClick(TObject *Sender)
{
    TGlyph *x  = (TGlyph*)FSel->Controls->Items[0];
	x->ImageIndex = ((int)x->ImageIndex >= dm->il->Count - 1) ? 0 : (int)x->ImageIndex + 1;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buImageRndClick(TObject *Sender)
{
    TGlyph *x  = (TGlyph*)FSel->Controls->Items[0];
	x->ImageIndex = Random(dm->il->Count);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ComboColorBoxChange(TObject *Sender)
{
	TRectangle *x = (TRectangle*)FSel->Controls->Items[0];
    x->Fill->Color = ComboColorBox->Color;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::trRectRadiusChange(TObject *Sender)
{
	TRectangle *x = (TRectangle*)FSel->Controls->Items[0];
	x->XRadius = trRectRadius->Value;
    x->YRadius = x->XRadius;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNewImgClick(TObject *Sender)
{
	TSelection *x = new TSelection(lay);
	x->Parent = lay;
	x->GripSize = 5;
	x->Width = 50 + Random(100);
	x->Height = 50 + Random(100);
	x->Position->X = Random(lay->Width - x ->Width);
	x->Position->Y = Random(lay->Height - x ->Height);
	x->RotationAngle = Random(100) - 50;
	x->OnMouseDown = SelectionAllMouseDown;
	TGlyph *xGlyph = new TGlyph(x);
	xGlyph->Parent = x;
	xGlyph->Align = TAlignLayout::Client;
	xGlyph->Images = dm->il;
	xGlyph->ImageIndex = Random(dm->il->Count);

	SelectionAll(x);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNewRectClick(TObject *Sender)
{
    TSelection *x = new TSelection(lay);
	x->Parent = lay;
	x->GripSize = 5;
	x->Width = 50 + Random(100);
	x->Height = 50 + Random(100);
	x->Position->X = Random(lay->Width - x ->Width);
	x->Position->Y = Random(lay->Height - x ->Height);
	x->RotationAngle = Random(100) - 50;
	x->OnMouseDown = SelectionAllMouseDown;
	TRectangle *xRectangle = new TRectangle(x);
	xRectangle->Parent = x;
	xRectangle->Align = TAlignLayout::Client;
	xRectangle->HitTest = false;
	xRectangle->XRadius = Random(50);
	xRectangle->YRadius = xRectangle->XRadius;
	xRectangle->Fill->Color = TAlphaColorF::Create(Random(256), Random(256), Random(256)).ToAlphaColor();

	SelectionAll(x);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buClearClick(TObject *Sender)
{
	SelectionAll(lay);
	for (int i = lay->ControlsCount - 1; i >= 0; i--) {
		if (dynamic_cast<TSelection*>(lay->Controls->Items[i])) {
            lay->RemoveObject(i);
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::trTextSizeChange(TObject *Sender)
{
	la->TextSettings->Font->Size = trTextSize->Value;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::edKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
    la->Text = ed->Text;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::trTextColorChange(TObject *Sender)
{
	la->TextSettings->FontColor = TAlphaColorF::Create(trTextColor->Value, trTextColor->Value, trTextColor->Value).ToAlphaColor();
}
//---------------------------------------------------------------------------

