//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Colors.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDEmanaged Components
	TButton *buNewImg;
	TButton *buClear;
	TButton *buNewRect;
	TButton *buAbout;
	TToolBar *tbHeader;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TLayout *lay;
	TSelection *Selection3;
	TRectangle *Rectangle;
	TToolBar *tbOptions;
	TButton *buBringToFront;
	TButton *buSentToBack;
	TTrackBar *trRotation;
	TButton *buDelete;
	TToolBar *tbRect;
	TComboColorBox *ComboColorBox;
	TTrackBar *trRectRadius;
	TButton *buImageRnd;
	TButton *buImageSelect;
	TToolBar *tbImage;
	TButton *buImagePrev;
	TButton *buImageNext;
	TLabel *la;
	TSelection *Selection4;
	TToolBar *tbText;
	TTrackBar *trTextSize;
	TEdit *ed;
	TTrackBar *trTextColor;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall trRotationChange(TObject *Sender);
	void __fastcall buDeleteClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageRndClick(TObject *Sender);
	void __fastcall ComboColorBoxChange(TObject *Sender);
	void __fastcall trRectRadiusChange(TObject *Sender);
	void __fastcall buNewImgClick(TObject *Sender);
	void __fastcall buNewRectClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall trTextSizeChange(TObject *Sender);
	void __fastcall edKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift);
	void __fastcall trTextColorChange(TObject *Sender);
private:	// User declarations
	TSelection *FSel;
	void SelectionAll(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
