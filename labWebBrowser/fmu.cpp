//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buGoClick(TObject *Sender)
{
	wb->URL =  edURL -> Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage("qqq");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buBackClick(TObject *Sender)
{
	wb->GoBack();

}
//---------------------------------------------------------------------------
void __fastcall TForm1::buForwardClick(TObject *Sender)
{
	wb->GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buReloadClick(TObject *Sender)
{
	wb->Reload();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStopClick(TObject *Sender)
{
	wb->Stop();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if(Key == vkReturn)
	{
	wb->URL =  edURL -> Text;
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::wbDidFinishLoad(TObject *ASender)
{
	  edURL -> Text = wb->URL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
    wb->URL =  edURL -> Text;
}
//---------------------------------------------------------------------------
