//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TButton *Button1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *tiResult;
	TButton *buStart;
	TImage *Image1;
	TScrollBox *ScrollBox1;
	TLabel *Label2;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TScrollBox *ScrollBox2;
	TLabel *Label3;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TScrollBox *ScrollBox3;
	TLabel *Label4;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TButton *Button13;
	TButton *Button14;
	TMemo *me;
	TProgressBar *pb;
	TLabel *lb;
	TLabel *lb1;
	TLabel *lb2;
	TImage *imCorrect;
	TImage *imWrong;
	TTabItem *TabItem1;
	TGridPanelLayout *GridPanelLayout1;
	TLabel *Label5;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:
	int FCountCorrect;
    int FCountWrong;
	// User declarations
public:
	// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
