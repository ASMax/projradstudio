//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
		tc->First();
		tc->TabPosition = TTabPosition::None;
		pb->  Max = tc-> TabCount -2   ;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
		tc->Next();
	 FCountCorrect=0;
	 FCountWrong=0;
	 imCorrect->Visible = 0;
	imWrong->Visible = 0;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButtonAllClick(TObject *Sender)
{
	UnicodeString x;
	 if(((TControl *)Sender) -> Tag == 1)  {
	 x= L"�����" ;
	 FCountCorrect++;
	 }
	 else{
		  x= L"�� �����" ;
	  FCountWrong++;
	 }


   //	x = ((TControl *)Sender) -> Tag == 1 ? L"�����":L"�� �����";
	me->Lines->Add (Format(L"������ %s - %s ", ARRAYOFCONST((tc->ActiveTab->Text,x))));
	  tc->Next();

}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button14Click(TObject *Sender)
{
	me->Lines->Clear();
	tc->First();

}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	pb-> Value = tc->ActiveTab ->Index ;
	lb-> Text = Format(L"%d �� %d ", ARRAYOFCONST((tc->ActiveTab ->Index,tc->TabCount - 2)));
	lb1-> Text = Format(L"�����: %d ", ARRAYOFCONST((FCountCorrect)));
	lb2-> Text = Format(L"�� �����:%d ", ARRAYOFCONST((FCountWrong)));
	lb->Visible = (tc->ActiveTab != tiMenu)  &&(tc->ActiveTab!=tiResult);
	imCorrect->Visible = (FCountCorrect > FCountWrong);
	imWrong->Visible = !imCorrect->Visible;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
    ShowMessage("...");
}
//---------------------------------------------------------------------------
