//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Edit4Click(TObject *Sender)
{
	//
	int xCategoryID = ((TControl *)Sender)->Tag;
	TLocateOptions xLO;
	dm->FDTable1->Locate(dm->FDTable1ID->FieldName, xCategoryID, xLO);
	laQu->Text = IntToStr(((TControl *)Sender)->Tag )+") "+ dm->FDTable1QU->Value;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->First();
	tc->TabPosition = TTabPosition::None;


}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button4Click(TObject *Sender)
{
	 TLocateOptions xLO;
	for(int i=1;i<10;i++){
			dm->FDTable1->Locate(dm->FDTable1ID->FieldName, i, xLO);
			TEdit *comp = (TEdit*)FindComponent("edName" + IntToStr(i));
			comp -> Text =  dm -> FDTable1WORD -> Value;
		}
	laQu->Text = " ";
	tc->ActiveTab = tiPlay;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button6Click(TObject *Sender)
{
	for(int i=1;i<46;i++)
	 {
		TEdit *comp1 = (TEdit*)FindComponent("Edit" + IntToStr(i));
		comp1->Text = "";
	 }
	tc->ActiveTab=tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
	ShowMessage((edName1 -> Text) + " " + (edName2 -> Text)+ " " + (edName3 -> Text) + " " + (edName4 -> Text)+ " " + (edName5 -> Text) + " " + (edName6 -> Text)+ " " + (edName7 -> Text) + " " + (edName8 -> Text)+ " " + (edName9 -> Text) +" \n"
	+ (ed1 -> Text) + " " + (ed2 -> Text) + " " + (ed3 -> Text) + " " + (ed4 -> Text) + " " + (ed5 -> Text) + " " + (ed6 -> Text) + " " + (ed7 -> Text) + " " + (ed8 -> Text) + " " + (ed9 -> Text) );
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button7Click(TObject *Sender)
{
	//TEdit *ed1, *ed2, *ed3, *ed4, *ed5, *ed6, *ed7, *ed8, *ed9;
	ed1 -> Text = (Edit4 -> Text) + (Edit10 -> Text) + (Edit13 -> Text) + (Edit22 -> Text) + (Edit25 -> Text);
	ed2 -> Text = (Edit13 -> Text) + (Edit14 -> Text) + (Edit15 -> Text) + (Edit16 -> Text) + (Edit17 -> Text) + (Edit18 -> Text) + (Edit19 -> Text) + (Edit20 -> Text);
	ed3 -> Text = (Edit1 -> Text) + (Edit2 -> Text) + (Edit5 -> Text) + (Edit11 -> Text) + (Edit17 -> Text);
	ed4 -> Text = (Edit6 -> Text) + (Edit7 -> Text) + (Edit8 -> Text) + (Edit9 -> Text);
	ed5 -> Text = (Edit3 -> Text) + (Edit6 -> Text) + (Edit12 -> Text) + (Edit19 -> Text) + (Edit23 -> Text) + (Edit30 -> Text);
	ed6 -> Text = (Edit26 -> Text) + (Edit27 -> Text) + (Edit28 -> Text) + (Edit29 -> Text) + (Edit30 -> Text) + (Edit31 -> Text) + (Edit32 -> Text) + (Edit33 -> Text);
	ed7 -> Text = (Edit28 -> Text) + (Edit34 -> Text) + (Edit39 -> Text) + (Edit44 -> Text);
	ed8 -> Text = (Edit36 -> Text) + (Edit37 -> Text) + (Edit38 -> Text) + (Edit39 -> Text) + (Edit40 -> Text) + (Edit41 -> Text) + (Edit42 -> Text);
	ed9 -> Text = (Edit21 -> Text) + (Edit24 -> Text) + (Edit33 -> Text) + (Edit35 -> Text) + (Edit43 -> Text) + (Edit45 -> Text);
	 int n=0;

	 for(int i=1;i<10;i++)
	 {

		TEdit *comp1 = (TEdit*)FindComponent("ed" + IntToStr(i));
		TEdit *comp2 = (TEdit*)FindComponent("edName" + IntToStr(i));
		if((comp1 -> Text) == (comp2 -> Text)){ n++;}
	 }
	 if(n==9)
	 {
		tc->ActiveTab=tiWin;
	 }
	 else
	 {
		ShowMessage("�� ����� ����������!\n���������� ��� ���.");
	 }

}
//---------------------------------------------------------------------------

void __fastcall Tfm::Edit4Change(TObject *Sender)
{
	if((((TEdit *)Sender)->Text) =="")
	{
		((TEdit *)Sender)->Text = IntToStr(((TControl *)Sender)->Tag);
	}

}
//---------------------------------------------------------------------------

