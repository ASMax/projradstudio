//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
	FDConnection1->Connected = True;
}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnection1BeforeConnect(TObject *Sender)
{
	FDConnection1->Params->Values["DataBase"] = "..\\..\\DBCROSS.FDB";

}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnection1AfterConnect(TObject *Sender)
{
	quWord->Open();

}
//---------------------------------------------------------------------------

