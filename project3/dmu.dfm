object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 307
  Width = 320
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Users\VD\Desktop\project3\DBCROSS.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    BeforeConnect = FDConnection1BeforeConnect
    Left = 40
    Top = 88
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 176
    Top = 96
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 216
    Top = 40
  end
  object FDTable1: TFDTable
    Active = True
    Connection = FDConnection1
    UpdateOptions.UpdateTableName = 'TWORD'
    TableName = 'TWORD'
    Left = 32
    Top = 168
    object FDTable1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object FDTable1WORD: TWideStringField
      FieldName = 'WORD'
      Origin = 'WORD'
      FixedChar = True
      Size = 10
    end
    object FDTable1QU: TWideStringField
      FieldName = 'QU'
      Origin = 'QU'
      FixedChar = True
      Size = 200
    end
  end
  object quWord: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    tword.id,'
      '    tword.word,'
      '    tword.qu'
      'from tword')
    Left = 256
    Top = 104
  end
end
