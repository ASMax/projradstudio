//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Colors.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *ti1;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TColorButton *ColorButton1;
	TColorButton *ColorButton2;
	TColorButton *ColorButton3;
	TColorButton *ColorButton4;
	TTabItem *ti2;
	TGridPanelLayout *GridPanelLayout2;
	TColorButton *bu1;
	TColorButton *bu2;
	TColorButton *bu3;
	TColorButton *bu4;
	TLabel *Label2;
	TImage *Image1;
	TToolBar *ToolBar1;
	TButton *BuStart;
	TButton *Button2;
	TLabel *Label3;
	TTabItem *tiOver;
	TTabItem *tiWin;
	TButton *Button1;
	TLabel *Label4;
	TImage *Image2;
	TLabel *Label5;
	TImage *Image3;
	TToolBar *ToolBar2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TTimer *tm;
	TLabel *laTime;
	TLabel *Label6;
	TTimer *tm2;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TFloatAnimation *FloatAnimation3;
	TFloatAnimation *FloatAnimation4;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall bu1MouseEnter(TObject *Sender);
	void __fastcall bu1MouseLeave(TObject *Sender);
	void __fastcall BuStartClick(TObject *Sender);
	void __fastcall ColorButton1Click(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall tm2Timer(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);

private:	// User declarations
	int i;
    int t;
	int n;
	int k;
	int r;
	int R;
    void AniStart();
	TDateTime FTimeStart;
	void DoRand(int r,int R);
	void DoPlay();
	void DoNew();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	 int a[100];
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
