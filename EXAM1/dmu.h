//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDQuery *quCategory;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TIntegerField *quCategoryID;
	TWideStringField *quCategoryNAME;
	TIntegerField *quCategorySORT_INDEX;
	TFDQuery *quProduct;
	TIntegerField *quProductID;
	TIntegerField *quProductCATEGORY_ID;
	TWideStringField *quProductNAME;
	TBlobField *quProductIMAGE;
	TFDStoredProc *spFeedbackIns;
	void __fastcall FDPhysFBDriverLink1DriverCreated(TObject *Sender);
	void __fastcall FDConnection1AfterConnect(TObject *Sender);
	void __fastcall FDConnection1BeforeConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
    void FeedbackIns(UnicodeString aFIO, UnicodeString aPhone,
	UnicodeString aEmail, UnicodeString aNote);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
