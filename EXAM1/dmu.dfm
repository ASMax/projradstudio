object dm: Tdm
  OldCreateOrder = False
  Height = 360
  Width = 435
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Users\VD\Documents\EXAM1\BLINCHIKI.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    AfterConnect = FDConnection1AfterConnect
    BeforeConnect = FDConnection1BeforeConnect
    Left = 72
    Top = 40
  end
  object quCategory: TFDQuery
    IndexesActive = False
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    category.id,'
      '    category.name,'
      '    category.sort_index'
      'from category')
    Left = 64
    Top = 104
    object quCategoryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quCategorySORT_INDEX: TIntegerField
      FieldName = 'SORT_INDEX'
      Origin = 'SORT_INDEX'
    end
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    OnDriverCreated = FDPhysFBDriverLink1DriverCreated
    Left = 240
    Top = 120
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 264
    Top = 48
  end
  object quProduct: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.image'
      'from product')
    Left = 88
    Top = 168
    object quProductID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProductCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quProductNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
  end
  object spFeedbackIns: TFDStoredProc
    Connection = FDConnection1
    Left = 240
    Top = 216
    ParamData = <
      item
        Position = 1
        Name = 'FIO'
        DataType = ftSingle
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftSingle
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'EMAIL'
        DataType = ftSingle
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'NOTE'
        DataType = ftSingle
        ParamType = ptInput
      end>
  end
end
