//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiBliny;
	TTabItem *tiProduct;
	TTabItem *tiBucket;
	TTabItem *tiOrder;
	TTabItem *tiGame;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buBliny;
	TButton *buGame;
	TButton *buFeedBack;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TImage *Image1;
	TLayout *Layout1;
	TScrollBox *ScrollBox1;
	TGridLayout *glCategory;
	TButton *Button1;
	TLabel *Label2;
	TLayout *Layout2;
	TButton *Button10;
	TLabel *Label3;
	TScrollBox *ScrollBox2;
	TGridLayout *glProduct;
	TTabItem *tiFeedBack;
	TTabItem *tiContact;
	TLayout *Layout3;
	TButton *Button2;
	TLabel *Label4;
	TLayout *Layout4;
	TEdit *edFeedbackFIO;
	TEdit *edFeedbackPhone;
	TEdit *edFeedbackEmail;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *LA;
	TLabel *Label8;
	TVertScrollBox *VertScrollBox1;
	TMemo *meFeedbackNote;
	TButton *Button3;
	TLayout *Layout5;
	TButton *Button11;
	TLabel *Label7;
	TLayout *Layout6;
	TButton *Button12;
	TLabel *Label9;
	TLayout *Layout7;
	TButton *Button13;
	TLabel *Label10;
	TLayout *Layout8;
	TButton *Button14;
	TLabel *Label11;
	TLayout *Layout9;
	TLabel *Label12;
	TStyleBook *StyleBook1;
	TStyleBook *StyleBook2;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBlinyClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall glCategoryClick(TObject *Sender);
	void __fastcall buGameClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall buFeedBackClick(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
private:	// User declarations

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	  void ReloadCategoryList() ;
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
