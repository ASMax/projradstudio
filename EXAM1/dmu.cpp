//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
#include "fmu.h"
#include "fruCategory.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDPhysFBDriverLink1DriverCreated(TObject *Sender)
{
	FDConnection1->Connected = True;
}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnection1AfterConnect(TObject *Sender)
{
	quCategory->Open();
    quProduct->Open();

}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnection1BeforeConnect(TObject *Sender)
{
    FDConnection1->Params->Values["DataBase"] = "..\\..\\BLINCHIKI.FDB";
}
//---------------------------------------------------------------------------

void Tdm::FeedbackIns(UnicodeString aFIO, UnicodeString aPhone,
	UnicodeString aEmail, UnicodeString aNote)
{
	spFeedbackIns->ParamByName("FIO")->Value = aFIO;
	spFeedbackIns->ParamByName("PHONE")->Value = aPhone;
	spFeedbackIns->ParamByName("EMAIL")->Value = aEmail;
	spFeedbackIns->ParamByName("NOTE")->Value = aNote;
	spFeedbackIns->ExecProc();
}
