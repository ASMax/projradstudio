//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "fruCategory.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
	
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->First();
	tc->TabPosition = TTabPosition::None;

}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBlinyClick(TObject *Sender)
{
	tc->ActiveTab = tiBliny;

}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
		tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormShow(TObject *Sender)
{
    dm->quCategory->First();
	glCategory->ItemHeight = 60;
	glCategory->ItemWidth = 150;
	while (!dm->quCategory->Eof) {
		TfrCategory *x = new TfrCategory(glCategory);
		x->Parent = glCategory;
		x->Align = TAlignLayout::Client;
		x->Name = "frCategory" + IntToStr(dm->quCategoryID->Value);
		x->la->Text = dm->quCategoryNAME->Value;
		//x->im->Bitmap->Assign(dm->quCategoryIMAGE);
		x->Tag = dm->quCategoryID->Value;
		glCategory->OnClick = glCategoryClick;
		//x->glCategory->OnClick = glCategoryClick;
		x->Locked = false;
        x->Enabled = true;
		x->Margins->Bottom = 10;
		x->Margins->Left = 10;
		x->Margins->Right = 10;
		x->Margins->Top = 10;
		dm->quCategory->Next();
	}
	glCategory->RecalcSize();
	glProduct->ItemHeight = 60;
	glProduct->ItemWidth = 150;
	while (!dm->quProduct->Eof) {
		TfrCategory *y = new TfrCategory(glProduct);
		y->Parent = glProduct;
		y->Align = TAlignLayout::Client;
		y->Name = "frProduct" + IntToStr(dm->quProductID->Value);
		y->la->Text = dm->quProductNAME->Value;
		y->im->Bitmap->Assign(dm->quProductIMAGE);
		y->Tag = dm->quProductID->Value;
		//x->OnClick = CategoryCellOnClick;
		dm->quProduct->Next();
	}
	glProduct->RecalcSize();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::glCategoryClick(TObject *Sender)
{
    int xCategoryID = ((TControl *)Sender)->Tag;
	TLocateOptions xLO;
	dm->quCategory->Locate(dm->quCategoryID->FieldName, xCategoryID, xLO);
    tc->GotoVisibleTab(tiProduct->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buGameClick(TObject *Sender)
{
       tc->ActiveTab = tiGame;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button3Click(TObject *Sender)
{
	 dm->FeedbackIns((edFeedbackFIO->Text),
	 (edFeedbackPhone->Text),
	 (edFeedbackEmail->Text),
	 (meFeedbackNote->Text));
	ShowMessage("����� ���������!");
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button4Click(TObject *Sender)
{
	   tc->ActiveTab = tiFeedBack;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFeedBackClick(TObject *Sender)
{
	  tc->ActiveTab = tiBucket;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
    tc->ActiveTab = tiContact;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button6Click(TObject *Sender)
{
    tc->ActiveTab = tiOrder;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button8Click(TObject *Sender)
{
    if (((TControl *)Sender)->Tag == 0) {
		fm->StyleBook = StyleBook1;
		((TControl *)Sender)->Tag = 1;
	}
		else{

		if (((TControl *)Sender)->Tag == 1)
			{
			fm->StyleBook = StyleBook2;
			((TControl *)Sender)->Tag = 2;
			}   else{

					fm->StyleBook = NULL;
					((TControl *)Sender)->Tag = 0;

				}
		}
}
//---------------------------------------------------------------------------

