//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
    db->FDConnection1 ->Connected = true;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAddClick(TObject *Sender)
{
	db->taNotes->Append();
	tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------

